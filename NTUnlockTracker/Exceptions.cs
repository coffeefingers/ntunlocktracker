﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTUnlockTracker
{
    class StreamKeyNotFoundException : Exception
    {
        public StreamKeyNotFoundException() : base() { }

        public StreamKeyNotFoundException(string message) : base(message) { }

        public StreamKeyNotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}
