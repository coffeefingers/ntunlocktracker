﻿using System.Drawing;
using System.Drawing.Imaging;

namespace NTUnlockTracker
{
    static class ImageUtil
    {
        static ColorMatrix grayscale;
        static ColorMatrix sepia;
        static ColorMatrix transparency;

        public static Bitmap ApplyGrayscale(Bitmap original, float brightness = -0.15f)
        {
            if (grayscale == null)
                grayscale = new ColorMatrix(new float[][] {
                                    new float[] { .3f, .3f, .3f, 0, 0 },
                                    new float[] { .59f, .59f, .59f, 0, 0 },
                                    new float[] { .11f, .11f, .11f, 0, 0 },
                                    new float[] { 0, 0, 0, 1, 0 },
                                    new float[] { brightness, brightness, brightness, 0, 1 }
                                });

            return ApplyFilterToBitmap(original, grayscale);
        }

        public static Bitmap ApplySepia(Bitmap original)
        {
            if (sepia == null)
                sepia = new ColorMatrix(new float[][] {
                                new float[] {.393f, .349f, .272f, 0, 0 },
                                new float[] {.769f, .686f, .534f, 0, 0 },
                                new float[] {.189f, .168f, .131f, 0, 0 },
                                new float[] {0, 0, 0, 1, 0 },
                                new float[] {0, 0, 0, 0, 1 }
                            });

            return ApplyFilterToBitmap(original, sepia);
        }

        public static Bitmap ApplyTransparency(Bitmap original, float alpha = 0.5f)
        {
            if (transparency == null)
                transparency = new ColorMatrix(new float[][] {
                                        new float[] {1, 0, 0, 0, 0},
                                        new float[] {0, 1, 0, 0, 0},
                                        new float[] {0, 0, 1, 0, 0},
                                        new float[] {0, 0, 0, alpha, 0},
                                        new float[] {0, 0, 0, 0, 1}
                                    });

            return ApplyFilterToBitmap(original, transparency);
        }

        public static Bitmap ApplyFilterToBitmap(Bitmap original, ColorMatrix matrix)
        {
            if (original != null)
            {
                Bitmap filtered = new Bitmap(original.Width, original.Height);
                ImageAttributes attribs = new ImageAttributes();

                attribs.SetColorMatrix(matrix);

                using (Graphics g = Graphics.FromImage(filtered))
                {
                    g.DrawImage(
                        original,
                        new Rectangle(0, 0, original.Width, original.Height),
                        0,
                        0,
                        original.Width,
                        original.Height,
                        GraphicsUnit.Pixel,
                        attribs);
                }

                return filtered;
            }
            return null;
        }
    }
}
