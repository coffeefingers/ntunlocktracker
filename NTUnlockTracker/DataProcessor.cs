﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NTUnlockTracker
{
    class DataProcessor
    {
        private const string _NT = "nuclearthrone";
        private const string _URL = "http://nuclearthrone.com/data/players/data/{0}{1}.cur";
        private const string _U98_URL = "https://tb-api.xyz/stream/get?s={0}&key={1}";

        private string _path;

        private string _steamID;
        private string _streamKey;
        private string _dataURL;
        private int _requestDelay;

        private CharacterData _charData;
        public CharacterData CharData { get { return _charData; } }

        private bool _useBetaUrl;
        public bool UseBetaUrl {
            get { return _useBetaUrl; }
            set {
                if (_useBetaUrl != value)
                {
                    _useBetaUrl = value;
                    _dataURL = String.Format(_U98_URL, _steamID, _streamKey);
                }
            }
        }

        public event DataUpdateHandler DataUpdated;

        public DataProcessor()
        {
            _charData = new CharacterData();
            DataUpdated += delegate { };

            InitSettings();
        }

        private void InitSettings()
        {
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            try
            {
                JObject jo;

                _path = Path.Combine(appData, _NT, (_NT + ".sav"));
                _requestDelay = Int32.Parse(ConfigurationManager.AppSettings["RequestDelay"] ?? "15");

                using (StreamReader sr = new StreamReader(_path))
                    jo = JObject.Parse(sr.ReadToEnd());

                _steamID = ConfigurationManager.AppSettings["SteamID"] ?? String.Empty;

                _streamKey = (jo["options"]["streamkey"]).ToString().ToUpper();
                if (String.IsNullOrEmpty(_streamKey))
                    throw new StreamKeyNotFoundException("Could not parse stream key! Is it set in game options?");

                _dataURL = String.Format(_URL, _steamID, _streamKey);
            }
            catch (StreamKeyNotFoundException)
            {
                
            }
            catch
            {
                
            }
        }

        public void UpdateSteamId(string id)
        {
            Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            KeyValueConfigurationCollection appConfig = configFile.AppSettings.Settings;

            _steamID = id;

            if (appConfig["SteamID"] == null)
                appConfig.Add("SteamID", _steamID);
            else
                appConfig["SteamID"].Value = _steamID;

            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            string url = (_useBetaUrl) ? _U98_URL : _URL;
            _dataURL = String.Format(url, _steamID, _streamKey);
        }

        public async void ProcessData(CancellationToken ct)
        {
            while (true)
            {
                ct.ThrowIfCancellationRequested();
                if (!String.IsNullOrEmpty(_streamKey) && !String.IsNullOrEmpty(_steamID))
                {
                    Console.WriteLine("Init data processing..." + DateTime.Now);
                    if (ParseStreamData())
                        ParseSaveData();
                    else
                        Console.WriteLine("Failed to parse stream data, skipping save data parsing...");
                }
                else
                {
                    Console.WriteLine("Skip data processing..." + DateTime.Now);
                }
                DataUpdated();
                await Task.Delay(_requestDelay * 1000);
            }
        }

        private bool ParseStreamData()
        {
            string streamData = String.Empty;
            JObject jo = null;
            bool success = false;

            try
            {
                using (WebClient wc = new WebClient())
                {
                    streamData = wc.DownloadString(_dataURL);
                }

                jo = JObject.Parse(streamData);
                //foreach (JValue val in jo.Descendants().OfType<JValue>().Where(v => v.Type == JTokenType.Null).ToList())
                //    val.Replace(String.Empty);

                if (_useBetaUrl) {
                    Console.WriteLine(jo);
                }

                _charData.Mutie = (Mutant)(int)jo["current"]["char"];
                int bSkin = (int)jo["current"]["skin"];
                _charData.UsingBSkin = (bSkin == 1);
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't parse stream data: " + ex.Message);
            }
            return success;
        }

        private void ParseSaveData()
        {
            string saveData = String.Empty;
            string charKey = "charData_" + (int)(_charData.Mutie);
            JObject jo;
            JToken jt;

            try
            {
                using (StreamReader sr = new StreamReader(_path))
                    saveData = sr.ReadToEnd();

                jo = JObject.Parse(saveData);
                jt = jo["stats"][charKey];

                //Console.WriteLine(jt);

                _charData.BSkin = (bool)jt["cbgt"];

                for (int i = 0; i < _charData.Crowns.Length; ++i)
                {
                    int j = (int)_charData.Crowns[i].Name;
                    _charData.Crowns[i].Locked = !(bool)jt["crowns"][j];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't parse save data: " + ex.Message);
            }
        }
    }
}
