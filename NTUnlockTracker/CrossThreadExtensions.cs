﻿using System;
using System.Windows.Forms;

namespace NTUnlockTracker
{
    public static class CrossThreadExtensions
    {
        public static void UpdateSafely(this Control target, Action action)
        {
            if (target.InvokeRequired)
            {
                target.Invoke(action);
            }
            else
            {
                action();
            }
        }
    }
}
