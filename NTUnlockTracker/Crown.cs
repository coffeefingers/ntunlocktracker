﻿namespace NTUnlockTracker
{
    class Crown
    {
        public const int NUM_CROWNS = 12;

        public CrownName Name { get; set; }
        public bool Locked { get; set; }

        public Crown(CrownName name)
        {
            this.Name = name;
            this.Locked = true;
        }
    }

    enum CrownName : byte
    {
        None = 0,
        Death = 2,
        Life,
        Haste,
        Guns,
        Hatred,
        Blood,
        Destiny,
        Love,
        Luck,
        Curses,
        Risk,
        Protection
    }
}
