﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Resources;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NTUnlockTracker
{
    public partial class FormMain : Form
    {
        private DataProcessor _dp;
        private CancellationToken _ct;
        private ResourceManager _rm;
        private PrivateFontCollection _pfc;

        private List<Label> _crowns;
        
        public FormMain(string[] args)
        {
            InitializeComponent();

            _dp = new DataProcessor();
            _dp.DataUpdated += UpdateUI;
            _ct = new CancellationToken();
            _rm = Properties.Resources.ResourceManager;

            ParseParameters(args);

            _crowns = new List<Label>();
        }

        private void ParseParameters(string[] args)
        {
            foreach (string arg in args)
            {
                switch (arg)
                {
                    case "/useBetaUrl":
                        _dp.UseBetaUrl = true;
                        break;
                }
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            InitCustomFont();
            InitCrownLabels();

            Task.Run(() => _dp.ProcessData(_ct), _ct);
        }

        private void InitCustomFont()
        {
            _pfc = new PrivateFontCollection();

            int fontLength = Properties.Resources.prstartk.Length;
            byte[] fontData = Properties.Resources.prstartk;

            IntPtr data = Marshal.AllocCoTaskMem(fontLength);
            Marshal.Copy(fontData, 0, data, fontLength);

            _pfc.AddMemoryFont(data, fontLength);

            Marshal.FreeCoTaskMem(data);

            List<Label> labels = pnlContent.Controls.OfType<Label>().ToList();
            foreach (Label label in labels)
                label.Font = new Font(_pfc.Families[0], label.Font.Size);
        }

        private void InitCrownLabels()
        {
            int baseX = 190;
            int baseY = 66;
            int lblSize = 36;
            int lblPadding = 4;

            for (int k = 0; k < Crown.NUM_CROWNS; ++k)
            {
                int iterator = (k % (Crown.NUM_CROWNS / 2)); // Should always be in range [0, 5]
                int offsetX = iterator * lblSize;
                int selector = (k / (Crown.NUM_CROWNS / 2)); // Should always be in range [0, 1]
                int offsetY = selector * 61;

                Point location = new Point() {
                                        X = (baseX + offsetX + lblPadding),
                                        Y = (baseY + offsetY)
                                    };

                Crown crown = _dp.CharData.Crowns[k];
                Bitmap image = _rm.GetObject(crown.Name.ToString()) as Bitmap;

                if (crown.Locked)
                    image = ImageUtil.ApplyTransparency(image);

                Label label = new Label() {
                                    AutoSize = false,
                                    Size = new Size(lblSize, lblSize),
                                    BackColor = Color.Transparent,
                                    Image = image,
                                    ImageAlign = ContentAlignment.MiddleCenter,
                                    Location = location,
                                    Visible = true,
                                    Tag = k // Used later to reference the associated Crown object in CharacterData
                                };
                label.MouseClick += CrownLabel_MouseClick;
                toolTips.SetToolTip(label, "Crown of " + crown.Name.ToString());

                _crowns.Add(label);
            }
            pnlContent.Controls.AddRange(_crowns.ToArray());
        }

        private void CrownLabel_MouseClick(object sender, MouseEventArgs e)
        {
            Label label = sender as Label;

            if (label != default(Label))
            {
                int which = (int)label.Tag;
                Crown crown = _dp.CharData.Crowns[which];

                if (crown != default(Crown))
                {
                    crown.Locked = !crown.Locked;
                    Image image = _rm.GetObject(crown.Name.ToString()) as Image;
                    if (crown.Locked)
                        label.Image = ImageUtil.ApplyTransparency(image as Bitmap);
                    else
                        label.Image = image;
                }
            }
        }

        private void PanelContent_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    //Task.Run(() => _dp.ProcessData(), _ct);
                    break;

                case MouseButtons.Right:
                    pnlGetSteamID.Visible = true;
                    break;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _dp.UpdateSteamId(tbSteamID.Text);
            pnlGetSteamID.Visible = false;
        }

        private void UpdateUI()
        {
            UpdateMutantImage();
            UpdateBSkinUnlocked();
            UpdateCrownImages();
        }

        private void UpdateMutantImage()
        {
            Mutant mutie = _dp.CharData.Mutie;
            bool bSkin = _dp.CharData.UsingBSkin;
            string imgName = mutie.ToString() + (bSkin ? "B" : "A");

            Image image = _rm.GetObject(imgName) as Image;

            if (image != default(Image))
                lblCharacter.UpdateSafely(() => {
                                                    lblCharacter.Image = image;
                                                    lblCharacter.Text = null;
                                                });
            else
            {
                lblCharacter.UpdateSafely(() => {
                                                    lblCharacter.Image = null;
                                                    lblCharacter.Text = "NO\nDATA";
                                                });
            }
        }

        private void UpdateBSkinUnlocked()
        {
            bool bSkin = _dp.CharData.BSkin;

            if (bSkin)
                lblBSkin.UpdateSafely(() => {
                    lblBSkin.Text = "B-SKIN UNLOCKED";
                    lblBSkin.ForeColor = Color.White;
                });
            else
                lblBSkin.UpdateSafely(() => {
                    lblBSkin.Text = "B-SKIN LOCKED";
                    lblBSkin.ForeColor = Color.Gray;
                });
        }

        private void UpdateCrownImages()
        {
            for (int i = 0; i < _crowns.Count; ++i)
            {
                Crown crown = _dp.CharData.Crowns[i];
                Bitmap image = _rm.GetObject(crown.Name.ToString()) as Bitmap;

                if (crown.Locked)
                    _crowns[i].UpdateSafely(() => _crowns[i].Image = ImageUtil.ApplyTransparency(image));
                else
                    _crowns[i].UpdateSafely(() => _crowns[i].Image = image);
            }
        }
    }
}
