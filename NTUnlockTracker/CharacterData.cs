﻿namespace NTUnlockTracker
{
    delegate void DataUpdateHandler();

    class CharacterData
    {
        public Mutant Mutie;
        public bool BSkin;
        public bool UsingBSkin;
        public bool GoldWeapon;
        public Crown[] Crowns;  // Gives me a way to access properties for each crown.


        public CharacterData(Mutant which = Mutant.None)
        {
            Mutie = which;
            BSkin = false;
            UsingBSkin = false;
            GoldWeapon = false;

            Crowns = new Crown[] {
                            new Crown(CrownName.Life),
                            new Crown(CrownName.Guns),
                            new Crown(CrownName.Haste),
                            new Crown(CrownName.Destiny),
                            new Crown(CrownName.Curses),
                            new Crown(CrownName.Risk),
                            new Crown(CrownName.Death),
                            new Crown(CrownName.Blood),
                            new Crown(CrownName.Hatred),
                            new Crown(CrownName.Love),
                            new Crown(CrownName.Luck),
                            new Crown(CrownName.Protection)
                        };
        }
    }

    enum Mutant : byte
    {
        None = 0,
        Fish = 1,
        Crystal,
        Eyes,
        Melting,
        Plant,
        YV,
        Steroids,
        Robot,
        Chicken,
        Rebel,
        Horror,
        Rogue,
        BigDog,
        Skeleton,
        Frog,
        Cuz
    }
}
