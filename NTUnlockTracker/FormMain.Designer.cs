﻿namespace NTUnlockTracker
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.lblCrownsPost = new System.Windows.Forms.Label();
            this.lblCrownsPre = new System.Windows.Forms.Label();
            this.lblBSkin = new System.Windows.Forms.Label();
            this.lblCharacter = new System.Windows.Forms.Label();
            this.pnlGetSteamID = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbSteamID = new System.Windows.Forms.TextBox();
            this.lblEnterID = new System.Windows.Forms.Label();
            this.toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.pnlContent.SuspendLayout();
            this.pnlGetSteamID.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContent
            // 
            this.pnlContent.BackColor = System.Drawing.Color.Black;
            this.pnlContent.Controls.Add(this.lblCrownsPost);
            this.pnlContent.Controls.Add(this.lblCrownsPre);
            this.pnlContent.Controls.Add(this.lblBSkin);
            this.pnlContent.Controls.Add(this.lblCharacter);
            this.pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContent.ForeColor = System.Drawing.Color.White;
            this.pnlContent.Location = new System.Drawing.Point(0, 0);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(480, 196);
            this.pnlContent.TabIndex = 0;
            this.pnlContent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PanelContent_MouseClick);
            // 
            // lblCrownsPost
            // 
            this.lblCrownsPost.AutoSize = true;
            this.lblCrownsPost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCrownsPost.Location = new System.Drawing.Point(189, 110);
            this.lblCrownsPost.Name = "lblCrownsPost";
            this.lblCrownsPost.Size = new System.Drawing.Size(148, 20);
            this.lblCrownsPost.TabIndex = 5;
            this.lblCrownsPost.Text = "POST-LOOP CROWNS";
            this.lblCrownsPost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCrownsPost.UseCompatibleTextRendering = true;
            // 
            // lblCrownsPre
            // 
            this.lblCrownsPre.AutoSize = true;
            this.lblCrownsPre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCrownsPre.Location = new System.Drawing.Point(189, 49);
            this.lblCrownsPre.Name = "lblCrownsPre";
            this.lblCrownsPre.Size = new System.Drawing.Size(139, 20);
            this.lblCrownsPre.TabIndex = 4;
            this.lblCrownsPre.Text = "PRE-LOOP CROWNS";
            this.lblCrownsPre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCrownsPre.UseCompatibleTextRendering = true;
            // 
            // lblBSkin
            // 
            this.lblBSkin.AutoSize = true;
            this.lblBSkin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBSkin.Location = new System.Drawing.Point(189, 19);
            this.lblBSkin.Name = "lblBSkin";
            this.lblBSkin.Size = new System.Drawing.Size(49, 20);
            this.lblBSkin.TabIndex = 3;
            this.lblBSkin.Text = "B-SKIN";
            this.lblBSkin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBSkin.UseCompatibleTextRendering = true;
            // 
            // lblCharacter
            // 
            this.lblCharacter.BackColor = System.Drawing.Color.Black;
            this.lblCharacter.Location = new System.Drawing.Point(0, 0);
            this.lblCharacter.Name = "lblCharacter";
            this.lblCharacter.Size = new System.Drawing.Size(196, 196);
            this.lblCharacter.TabIndex = 2;
            this.lblCharacter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCharacter.UseCompatibleTextRendering = true;
            // 
            // pnlGetSteamID
            // 
            this.pnlGetSteamID.BackColor = System.Drawing.Color.Black;
            this.pnlGetSteamID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGetSteamID.Controls.Add(this.btnSave);
            this.pnlGetSteamID.Controls.Add(this.tbSteamID);
            this.pnlGetSteamID.Controls.Add(this.lblEnterID);
            this.pnlGetSteamID.ForeColor = System.Drawing.Color.White;
            this.pnlGetSteamID.Location = new System.Drawing.Point(108, 84);
            this.pnlGetSteamID.Name = "pnlGetSteamID";
            this.pnlGetSteamID.Size = new System.Drawing.Size(264, 28);
            this.pnlGetSteamID.TabIndex = 2;
            this.pnlGetSteamID.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.AutoSize = true;
            this.btnSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(218, 1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(42, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbSteamID
            // 
            this.tbSteamID.AcceptsReturn = true;
            this.tbSteamID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSteamID.Location = new System.Drawing.Point(92, 3);
            this.tbSteamID.MaxLength = 32;
            this.tbSteamID.Name = "tbSteamID";
            this.tbSteamID.Size = new System.Drawing.Size(122, 20);
            this.tbSteamID.TabIndex = 1;
            this.tbSteamID.UseSystemPasswordChar = true;
            // 
            // lblEnterID
            // 
            this.lblEnterID.AutoSize = true;
            this.lblEnterID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnterID.Location = new System.Drawing.Point(0, 6);
            this.lblEnterID.Name = "lblEnterID";
            this.lblEnterID.Size = new System.Drawing.Size(93, 13);
            this.lblEnterID.TabIndex = 0;
            this.lblEnterID.Text = "Enter Steam ID";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 196);
            this.Controls.Add(this.pnlGetSteamID);
            this.Controls.Add(this.pnlContent);
            this.Name = "FormMain";
            this.Text = "NT Unlock Tracker";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PanelContent_MouseClick);
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.pnlGetSteamID.ResumeLayout(false);
            this.pnlGetSteamID.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContent;
        private System.Windows.Forms.Label lblCharacter;
        private System.Windows.Forms.Label lblBSkin;
        private System.Windows.Forms.Label lblCrownsPre;
        private System.Windows.Forms.Panel pnlGetSteamID;
        private System.Windows.Forms.TextBox tbSteamID;
        private System.Windows.Forms.Label lblEnterID;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblCrownsPost;
        private System.Windows.Forms.ToolTip toolTips;
    }
}

